This project demonstrates basic functionality of a prototype ATM, against requirements provided by SailPoint (listed below).

Requirements:

    - Gradle 6.8.x, 6.9.x, or 7.x.

To Build: from the commandline, and within the root directory, invoke Gradle to build the code:
    `./gradlew clean build`

To execute the .jar, use the following command (again, still in the root directory):
    ` java -jar .\build\libs\robertpace-0.0.1-SNAPSHOT.jar`

This will use begin a SpringBoot application, and expose several REST endpoints at `localhost:8080`

To assist in the testing of the application, there is a **Postman collection** included, to show the different ways the REST endpoints will respond.

The following three endpoints are available:
    
    endpoint: /accounts
    call type: GET
    intention: retrieve and display accounts for a Customer based on their PIN
    inputs
        type: Header
        name: 'pin'
        value: 4-digit String
    example: localhost:8080/accounts
    

    endpoint: /accounts
    call type: POST
    intention: allow a customer to "operate" on one of their accounts (i.e., deposit or withdraw a certain amount)
    inputs
        type: Header
        name: 'pin'
        value: 4-digit String

        type: QueryParam
        name: 'operation'
        value: String, either "deposit" or "withdraw"

        type: QueryParam
        name: 'account'
        value: String, either "checking" or "savings"

        type: QueryParam
        name: 'amount'
        value: String, can be a decimal-value
    example: localhost:8080/accounts?operation=deposit&account=checking&amount=10



        









