package com.sailpoint.robertpace.exceptions;

public class CustomerCreationException extends GenericException {
  private static final long serialVersionUID = -8729875L;

  public CustomerCreationException(String reason) {
    super(reason);
  }
}
