package com.sailpoint.robertpace.exceptions;

public class AccountRetreivalPinMalformedException extends GenericException {
  private static final long serialVersionUID = -2392792782L;

  public AccountRetreivalPinMalformedException(String reason) {
    super(reason);
  }
}
