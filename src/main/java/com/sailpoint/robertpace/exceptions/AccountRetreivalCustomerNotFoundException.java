package com.sailpoint.robertpace.exceptions;

public class AccountRetreivalCustomerNotFoundException extends GenericException {
  private static final long serialVersionUID = -239792782L;

  public AccountRetreivalCustomerNotFoundException(String reason) {
    super(reason);
  }
}
