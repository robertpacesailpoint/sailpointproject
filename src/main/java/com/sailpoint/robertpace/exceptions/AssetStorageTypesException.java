package com.sailpoint.robertpace.exceptions;

public class AssetStorageTypesException extends GenericException {
  private static final long serialVersionUID = -243690382L;

  public AssetStorageTypesException(String reason) {
    super(reason);
  }
}
