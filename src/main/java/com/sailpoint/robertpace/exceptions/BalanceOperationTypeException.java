package com.sailpoint.robertpace.exceptions;

public class BalanceOperationTypeException extends GenericException {
  private static final long serialVersionUID = -232625L;

  public BalanceOperationTypeException(String reason) {
    super(reason);
  }
}
