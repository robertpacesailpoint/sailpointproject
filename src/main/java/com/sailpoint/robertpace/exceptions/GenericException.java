package com.sailpoint.robertpace.exceptions;

import lombok.Getter;

public class GenericException extends Exception {
  private static final long serialVersionUID = -107239487928L;

  @Getter public String reason;

  public GenericException(String reason) {
    this.reason = reason;
  }
}
