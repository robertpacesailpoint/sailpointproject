package com.sailpoint.robertpace.exceptions;

public class UpdateAccountException extends GenericException {
  private static final long serialVersionUID = -9872349L;

  public UpdateAccountException(String reason) {
    super(reason);
  }
}
