package com.sailpoint.robertpace.resource;

import static com.sailpoint.robertpace.resource.AccountsResource.ROOT_PATH;

import com.sailpoint.robertpace.exceptions.*;
import com.sailpoint.robertpace.services.AccountService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(value = ROOT_PATH)
@RestController
@RequestMapping(value = ROOT_PATH)
@SuppressWarnings({"PMD.BeanMembersShouldSerialize"})
public class AccountsResource {

  public static final String ROOT_PATH = "/accounts";

  @Autowired AccountService accountService;

  @RequestMapping(method = {RequestMethod.GET})
  public ResponseEntity getAccounts(@RequestHeader("pin") String pin) {
    try {
      return ResponseEntity.status(HttpStatus.OK).body(accountService.getAccounts(pin));
    } catch (CustomerCreationException e) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getReason());
    } catch (AccountRetreivalPinMalformedException e) {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getReason());
    } catch (AccountRetreivalCustomerNotFoundException e) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getReason());
    }
  }

  @RequestMapping(method = {RequestMethod.POST})
  public ResponseEntity updateBalance(
      @RequestHeader("pin") final String pin,
      @RequestParam("operation") final String operation,
      @RequestParam("account") final String account,
      @RequestParam("amount") final String amount) {
    try {
      return ResponseEntity.status(HttpStatus.OK)
          .body(
              "Operation successful. New balance is "
                  + accountService.updateAccount(pin, operation, account, amount));
    } catch (BalanceOperationTypeException
        | AssetStorageTypesException
        | UpdateAccountException
        | CustomerCreationException e) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getReason());
    } catch (AccountRetreivalPinMalformedException e) {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getReason());
    } catch (AccountRetreivalCustomerNotFoundException e) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getReason());
    }
  }
}
