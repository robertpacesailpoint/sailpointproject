package com.sailpoint.robertpace.resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "liveness")
public class LivenessResource {
  @RequestMapping(method = {RequestMethod.GET})
  public ResponseEntity liveness() {
    return ResponseEntity.status(HttpStatus.OK).body("System is up!");
  }
}
