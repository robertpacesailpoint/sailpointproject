package com.sailpoint.robertpace.dto;

import com.sailpoint.robertpace.exceptions.CustomerCreationException;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

@Accessors(chain = true)
@Builder(builderClassName = "builder")
public class Customer {

  /*
  ID is used as a 'key' in the Customer Map, as a unique identifier.
  Otherwise, unused throughout the codebase.
   */
  String id;
  @Getter String pin;
  String friendlyName;
  @Getter List<Account> accounts;

  public static class builder {
    public static final String BAD_ID = "ID is missing in customer creation";
    public static final String BAD_PIN = "PIN is missing or malformed in customer creation";
    public static final String BAD_NAME = "Name is missing in customer creation";
    public static final String BAD_ACCOUNTS = "Accounts are is missing in customer creation";

    public Customer build() throws CustomerCreationException {
      if (!StringUtils.hasText(id)) {
        throw new CustomerCreationException(BAD_ID);
      }
      if (!StringUtils.hasText(pin) || pin.length() != 4) {
        throw new CustomerCreationException(BAD_PIN);
      }
      if (!StringUtils.hasText(friendlyName)) {
        throw new CustomerCreationException(BAD_NAME);
      }
      if (CollectionUtils.isEmpty(accounts)) {
        throw new CustomerCreationException(BAD_ACCOUNTS);
      }
      return new Customer(id, pin, friendlyName, accounts);
    }
  }
}
