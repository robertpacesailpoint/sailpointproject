package com.sailpoint.robertpace.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sailpoint.robertpace.enums.AssetStorageType;
import java.math.BigDecimal;
import java.text.NumberFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true)
public class Account {
  @Getter @Setter private AssetStorageType assetStorageType;

  // Don't show balances; We want to manage how it is  displayed.
  @JsonIgnore @Getter @Setter private BigDecimal balance;

  private String formattedBalance;

  public String getFormattedBalance() {
    return NumberFormat.getCurrencyInstance().format(balance);
  }
}
