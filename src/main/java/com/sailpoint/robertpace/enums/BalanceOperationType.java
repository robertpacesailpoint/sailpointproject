package com.sailpoint.robertpace.enums;

import com.sailpoint.robertpace.exceptions.BalanceOperationTypeException;
import java.util.Arrays;
import java.util.stream.Collectors;

public enum BalanceOperationType {
  DEPOSIT("deposit"),
  WITHDRAW("withdraw");

  private String friendlyCommand;

  BalanceOperationType(String friendlyCommand) {
    this.friendlyCommand = friendlyCommand;
  }

  public static BalanceOperationType getTypeFromFriendlyCommand(String input)
      throws BalanceOperationTypeException {
    for (final BalanceOperationType balanceOperationType : BalanceOperationType.values()) {
      if (balanceOperationType.friendlyCommand.equalsIgnoreCase(input)) {
        return balanceOperationType;
      }
    }
    throw new BalanceOperationTypeException(
        "Operation not understood. Available options are: " + getAvailableCommands());
  }

  public static String getAvailableCommands() {
    return Arrays.stream(BalanceOperationType.values())
        .map(Enum::toString)
        .collect(Collectors.joining(", "));
  }
}
