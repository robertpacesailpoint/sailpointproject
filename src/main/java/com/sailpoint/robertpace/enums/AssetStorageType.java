package com.sailpoint.robertpace.enums;

import com.sailpoint.robertpace.exceptions.AssetStorageTypesException;

public enum AssetStorageType {
  CHECKING("checking"),
  SAVINGS("savings");

  private String friendlyName;

  AssetStorageType(String friendlyName) {
    this.friendlyName = friendlyName;
  }

  public static AssetStorageType getTypeFromFriendlyName(String input)
      throws AssetStorageTypesException {
    for (final AssetStorageType assetStorageType : AssetStorageType.values()) {
      if (assetStorageType.friendlyName.equalsIgnoreCase(input)) {
        return assetStorageType;
      }
    }
    throw new AssetStorageTypesException("Account type not understood.");
  }
}
