package com.sailpoint.robertpace.services;

import com.sailpoint.robertpace.dto.Account;
import com.sailpoint.robertpace.dto.Customer;
import com.sailpoint.robertpace.enums.AssetStorageType;
import com.sailpoint.robertpace.enums.BalanceOperationType;
import com.sailpoint.robertpace.exceptions.*;
import java.math.BigDecimal;
import java.util.List;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@SuppressWarnings({"PMD.BeanMembersShouldSerialize", "PMD.DataflowAnomalyAnalysis"})
public class AccountService {

  public static final String INVALID_INPUT = "Input PIN was invalid.";
  public static final String NO_CUSTOMER_FOUND =
      "PIN was properly formed, but does not match any account.";
  public static final String MALFORMED_PIN = "PIN appears to be malformed.";
  public static final String BAD_OPERATION = "This operation is not allowed.";
  public static final String BAD_ACCOUNT = "Account type could not be understood.";
  public static final String DELTA_CANT_BE_NEGATIVE =
      "Operations to account balances cannot be changed by a negative amount.";
  public static final String DELTA_NOT_UNDERSTOOD =
      "Amount could not be interpreted into a dollar value.";
  public static final String ACCOUNT_NOT_FOUND = "No account found of this type for this customer.";
  public static final String BALANCE_WOULD_GO_NEGATIVE =
      "Cannot withdraw this amount... account balance would become negative. Balance remains: ";

  @Autowired CustomerService customerService;

  public List<Account> getAccounts(final String pin)
      throws AccountRetreivalCustomerNotFoundException, CustomerCreationException,
          AccountRetreivalPinMalformedException {
    if (!StringUtils.hasText(pin)) {
      throw new AccountRetreivalCustomerNotFoundException(INVALID_INPUT);
    }
    return getCustomerFromPin(pin).getAccounts();
  }

  public String updateAccount(
      final String pin,
      final String operationInput,
      final String accountInput,
      final String amountInput)
      throws UpdateAccountException, AccountRetreivalCustomerNotFoundException,
          BalanceOperationTypeException, AssetStorageTypesException, CustomerCreationException,
          AccountRetreivalPinMalformedException {

    BigDecimal amount;
    try {
      // in case the input contains a comma, remove it, because that would still a valid dollar
      // amount.
      amount = new BigDecimal(amountInput.trim().replaceAll("[,]", ""));
    } catch (Exception e) {
      throw new UpdateAccountException(DELTA_NOT_UNDERSTOOD);
    }

    if (!StringUtils.hasText(operationInput)) {
      throw new UpdateAccountException(BAD_OPERATION);
    }

    final BalanceOperationType balanceOperationType =
        BalanceOperationType.getTypeFromFriendlyCommand(operationInput);

    if (amount.compareTo(BigDecimal.ZERO) < 0) {
      throw new UpdateAccountException(DELTA_CANT_BE_NEGATIVE);
    }

    if (!StringUtils.hasText(accountInput)) {
      throw new UpdateAccountException(BAD_ACCOUNT);
    }

    final AssetStorageType assetStorageType =
        AssetStorageType.getTypeFromFriendlyName(accountInput);

    final List<Account> accounts = getCustomerFromPin(pin).getAccounts();
    for (final Account account : accounts) {
      if (account.getAssetStorageType() == assetStorageType) {
        if (balanceOperationType == BalanceOperationType.WITHDRAW) {
          amount = amount.negate();

          if (account.getBalance().add(amount).compareTo(BigDecimal.ZERO) < 0) {
            throw new UpdateAccountException(BALANCE_WOULD_GO_NEGATIVE + account.getBalance());
          }
        }

        account.setBalance(account.getBalance().add(amount));
        return account.getFormattedBalance();
      }
    }
    throw new UpdateAccountException(ACCOUNT_NOT_FOUND);
  }

  private Customer getCustomerFromPin(final String pin)
      throws AccountRetreivalCustomerNotFoundException, CustomerCreationException,
          AccountRetreivalPinMalformedException {

    if (pin.length() != 4 || !NumberUtils.isNumber(pin)) {
      throw new AccountRetreivalPinMalformedException(MALFORMED_PIN);
    }

    for (Customer customer : customerService.getCustomers().values()) {
      if (customer.getPin().equalsIgnoreCase(pin)) {
        return customer;
      }
    }
    throw new AccountRetreivalCustomerNotFoundException(NO_CUSTOMER_FOUND);
  }
}
