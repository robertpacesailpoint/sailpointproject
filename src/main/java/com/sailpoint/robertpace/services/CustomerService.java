package com.sailpoint.robertpace.services;

import com.sailpoint.robertpace.dto.Account;
import com.sailpoint.robertpace.dto.Customer;
import com.sailpoint.robertpace.enums.AssetStorageType;
import com.sailpoint.robertpace.exceptions.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {

  public static final String SAMPLE_ID = "1234";
  public static final String SAMPLE_PIN = "5678";
  public static final String SAMPLE_NAME = "Robert";
  public static final List<Account> SAMPLE_ACCOUNTS =
      new ArrayList() {
        {
          add(
              new Account()
                  .setAssetStorageType(AssetStorageType.CHECKING)
                  .setBalance(new BigDecimal("0")));
          add(
              new Account()
                  .setAssetStorageType(AssetStorageType.SAVINGS)
                  .setBalance(new BigDecimal("0")));
        }
      };

  public Map<String, Customer> getCustomers() throws CustomerCreationException {
    return new HashMap() {
      {
        put(
            SAMPLE_ID,
            Customer.builder()
                .id(SAMPLE_ID)
                .pin(SAMPLE_PIN)
                .friendlyName(SAMPLE_NAME)
                .accounts(SAMPLE_ACCOUNTS)
                .build());
      }
    };
  }
}
